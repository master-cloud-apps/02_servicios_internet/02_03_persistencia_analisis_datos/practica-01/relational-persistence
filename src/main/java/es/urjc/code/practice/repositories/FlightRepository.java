package es.urjc.code.practice.repositories;

import es.urjc.code.practice.models.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {

    @Query(value = "select f FROM Flight f WHERE f.destinationAirport.city = ?1 AND DATE_FORMAT(f.departureDate, " +
            "'%d/%m/%Y') = ?2 ORDER BY f.departureDate")
    List<Flight> findFlightsByDestinationCityAndDate(String city, String date);

}
