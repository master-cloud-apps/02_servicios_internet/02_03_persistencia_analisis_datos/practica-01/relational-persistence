package es.urjc.code.practice.repositories;

import es.urjc.code.practice.dtos.CabinCrewWithTotalFlightsAndHoursIDTO;
import es.urjc.code.practice.dtos.FlatCabinCrewWithFlightsDTO;
import es.urjc.code.practice.models.CabinCrew;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CabinCrewRepository extends JpaRepository<CabinCrew, Long> {

    @Query(value = "SELECT new es.urjc.code.practice.dtos.FlatCabinCrewWithFlightsDTO(cc.name, cc.surname, a.city, f.departureDate) " +
            "FROM CabinCrew cc LEFT JOIN FlightCabinCrew fcc ON cc=fcc.cabinCrew " +
            "LEFT JOIN Flight f ON f=fcc.flight " +
            "LEFT JOIN Airport a ON a=f.originAirport " +
            "WHERE cc.employeeCode = ?1")
    List<FlatCabinCrewWithFlightsDTO> findByEmployeeCodeWithFlights(String employeeCode);

    @Query(name= "cabinCrew.findAllCrewWithTotalFlights", nativeQuery = true)
    List<CabinCrewWithTotalFlightsAndHoursIDTO> findAllCrewWithTotalFlights();

}
