package es.urjc.code.practice.dtos;

import org.springframework.beans.factory.annotation.Value;

public interface FlatPlaneWithMechanicDTO {

    Long getId();
    String getRegistration();
    String getProducer();
    String getModel();
    Float getFlightHours();

    @Value("#{@mapperUtility.buildMechanicDTO(target.mechanicName, target.mechanicSurname)}")
    MechanicDTO getMechanic();
}
