package es.urjc.code.practice.dtos;

import org.springframework.stereotype.Component;

@Component
public class MapperUtility {

    public MechanicDTO buildMechanicDTO(String mechanicName, String mechanicSurname) {
        if (mechanicName != null && mechanicSurname != null)
            return new MechanicDTO(mechanicName, mechanicSurname);
        return null;
    }
}
