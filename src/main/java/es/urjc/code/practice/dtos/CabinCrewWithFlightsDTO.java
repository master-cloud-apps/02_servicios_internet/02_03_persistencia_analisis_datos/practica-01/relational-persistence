package es.urjc.code.practice.dtos;

import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public class CabinCrewWithFlightsDTO {

    private String name;
    private String surname;
    private List<FlightDTO> flights;

    public CabinCrewWithFlightsDTO(String name, String surname, List<FlightDTO> flights) {
        this.name = name;
        this.surname = surname;
        this.flights = flights;
    }
}
