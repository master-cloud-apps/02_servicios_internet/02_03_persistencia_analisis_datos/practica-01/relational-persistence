package es.urjc.code.practice.services;

import es.urjc.code.practice.dtos.CabinCrewWithFlightsDTO;
import es.urjc.code.practice.dtos.CabinCrewWithTotalFlightsAndHoursDTO;

import java.util.List;

public interface CabinCrewService {

    CabinCrewWithFlightsDTO findCitiesAndDatesByCrewEmployeeCode(String employeeCode);

    List<CabinCrewWithTotalFlightsAndHoursDTO> findAllCrewWithTotalFlights();
}
