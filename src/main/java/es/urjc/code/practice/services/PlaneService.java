package es.urjc.code.practice.services;

import es.urjc.code.practice.dtos.PlaneDTO;

import java.util.List;

public interface PlaneService {

    List<PlaneDTO> findPlanesAndMechanics();

}
