package es.urjc.code.practice.services.impl;

import es.urjc.code.practice.dtos.FlatPlaneWithMechanicDTO;
import es.urjc.code.practice.dtos.PlaneDTO;
import es.urjc.code.practice.repositories.PlaneRepository;
import es.urjc.code.practice.services.PlaneService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlaneServiceImpl implements PlaneService {

    private PlaneRepository planeRepository;

    public PlaneServiceImpl(PlaneRepository planeRepository) {
        this.planeRepository = planeRepository;
    }

    @Override
    public List<PlaneDTO> findPlanesAndMechanics() {
        return this.planeRepository.findPlaneMechanics()
                .stream()
                .collect(Collectors.toMap(
                        FlatPlaneWithMechanicDTO::getId,
                        flatPlane -> new PlaneDTO(flatPlane),
                        (existingOne, newOne) -> {
                            existingOne.getMechanics().addAll(newOne.getMechanics());
                            return existingOne;
                        }))
                .values()
                .stream()
                .collect(Collectors.toList());
    }

}
