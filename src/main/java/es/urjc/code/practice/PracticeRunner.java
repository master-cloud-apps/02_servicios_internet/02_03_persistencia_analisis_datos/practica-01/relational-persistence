package es.urjc.code.practice;

import es.urjc.code.practice.services.CabinCrewService;
import es.urjc.code.practice.services.FlightService;
import es.urjc.code.practice.services.PlaneService;
import es.urjc.code.practice.utils.DatabaseLoader;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Controller;


// TODO: Para el apartado 1 imprimir el contenido de todas las listas??
// TODO: documentar README.md
@Controller
public class PracticeRunner implements CommandLineRunner {

    private DatabaseLoader databaseLoader;
    private PlaneService planeService;
    private FlightService flightService;
    private CabinCrewService cabinCrewService;

    public PracticeRunner(DatabaseLoader databaseLoader,
                          PlaneService planeService,
                          FlightService flightService,
                          CabinCrewService cabinCrewService) {
        this.databaseLoader = databaseLoader;
        this.planeService = planeService;
        this.flightService = flightService;
        this.cabinCrewService = cabinCrewService;
    }

    @Override
    public void run(String... args) throws Exception {

        try {
            this.databaseLoader.setUp();

            System.out.println("Showing airplanes with their mechanics");
            System.out.println("------------------------------------------------------");
            this.planeService.findPlanesAndMechanics()
                    .stream()
                    .forEach(System.out::println);
            System.out.println("------------------------------------------------------\n\n");

            System.out.println("Showing flight by destination city and departure date sorted by hours");
            System.out.println("------------------------------------------------------");
            this.flightService
                    .findFlightsByDestinationCityInDate(DatabaseLoader.CITY_3,
                            this.databaseLoader.getSdf().parse(DatabaseLoader.DEPARTURE_DATE_1_WITH_HOUR))
                    .stream()
                    .forEach(System.out::println);
            System.out.println("------------------------------------------------------\n\n");

            System.out.println("Showing cabin crew by employee code with origin cities and departure date");
            System.out.println("------------------------------------------------------");
            System.out.println(this.cabinCrewService.findCitiesAndDatesByCrewEmployeeCode(DatabaseLoader.EMPLOYEE_CODE_1));
            System.out.println("------------------------------------------------------\n\n");

            System.out.println("Showing all cabin crew with their total number of flights and their total flight hours");
            System.out.println("------------------------------------------------------");
            this.cabinCrewService.findAllCrewWithTotalFlights()
                    .stream()
                    .forEach(System.out::println);
            System.out.println("------------------------------------------------------\n\n");

        } finally {
            this.databaseLoader.deleteAll();
        }

    }

}
