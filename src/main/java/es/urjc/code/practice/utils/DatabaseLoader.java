package es.urjc.code.practice.utils;

import es.urjc.code.practice.models.*;
import es.urjc.code.practice.repositories.*;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Getter
public class DatabaseLoader {

    public static final String PLANE_REGISTRATION_1 = "EC-AAA";
    public static final String PLANE_REGISTRATION_2 = "EC-AAB";
    public static final String PLANE_REGISTRATION_3 = "EC-AAC";
    public static final String PLANE_REGISTRATION_4 = "EC-AAD";

    private static final String COMPANY_NAME_1 = "Iberia";
    private static final String COMPANY_NAME_2 = "Vueling";
    private static final String COMPANY_NAME_3 = "Ryanair";

    public static final String CITY_1 = "Madrid";
    public static final String CITY_2 = "Valencia";
    public static final String CITY_3 = "Santiago";

    public static final String EMPLOYEE_CODE_0 = "EMP-000";
    public static final String EMPLOYEE_CODE_1 = "EMP-001";
    public static final String EMPLOYEE_CODE_2 = "EMP-002";
    public static final String EMPLOYEE_CODE_3 = "EMP-003";
    public static final String EMPLOYEE_CODE_4 = "EMP-004";
    public static final String EMPLOYEE_CODE_5 = "EMP-005";
    public static final String EMPLOYEE_CODE_6 = "EMP-006";
    public static final String EMPLOYEE_CODE_7 = "EMP-007";
    public static final String EMPLOYEE_CODE_8 = "EMP-008";
    public static final String EMPLOYEE_CODE_9 = "EMP-009";

    public static final String DEPARTURE_DATE_1 = "01/01/2021";
    public static final String DEPARTURE_DATE_1_WITH_HOUR = DEPARTURE_DATE_1 + " 00:00:00";

    public static final String FLIGHT_CODE_1 = "IB101";
    public static final String FLIGHT_CODE_2 = "VU101";
    public static final String FLIGHT_CODE_3 = "VU102";
    public static final String FLIGHT_CODE_4 = "VU103";
    public static final String FLIGHT_CODE_5 = "IB102";


    @Autowired
    private FlightRepository flightRepository;
    @Autowired
    private AirportRepository airportRepository;
    @Autowired
    private PlaneRepository planeRepository;
    @Autowired
    private CabinCrewRepository cabinCrewRepository;
    @Autowired
    private MechanicRepository mechanicRepository;
    @Autowired
    private MaintenanceRepository maintenanceRepository;


    private Map<String, Plane> planes = new HashMap<>();
    private Map<String, Mechanic> mechanics = new HashMap<>();
    private Map<String, Airport> airports = new HashMap<>();
    private List<CabinCrew> cabinCrew = new ArrayList<>();
    private Map<String, Flight> flights = new HashMap<>();

    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public void setUp() throws ParseException {
        System.out.println("\n\nInitializing database");
        System.out.println("------------------------------------------------------");

        this.createPlanes();
        this.createAirports();
        this.createMechanics();
        this.createMaintenances();
        this.createCabinCrew();
        this.createFlights();

        System.out.println("------------------------------------------------------\n\n");
    }

    public void deleteAll() {
        System.out.println("\n\nClearing database");
        System.out.println("------------------------------------------------------");
        this.maintenanceRepository.deleteAll();
        this.cabinCrewRepository.deleteAll();
        this.mechanicRepository.deleteAll();
        this.flightRepository.deleteAll();
        this.planeRepository.deleteAll();
        this.airportRepository.deleteAll();
        this.initEntities();
        System.out.println("------------------------------------------------------\n\n");
    }

    private void createPlanes() {
        this.planes.put(PLANE_REGISTRATION_1, new Plane(null, PLANE_REGISTRATION_1, "Boeing", "747", 245.73f));
        this.planes.put(PLANE_REGISTRATION_2, new Plane(null, PLANE_REGISTRATION_2, "Airbus", "A340", 320.73f));
        this.planes.put(PLANE_REGISTRATION_3, new Plane(null, PLANE_REGISTRATION_3, "Embraer", "100", 49.73f));
        this.planes.put(PLANE_REGISTRATION_4, new Plane(null, PLANE_REGISTRATION_4, "Bombardier", "CSeries", 194.37f));
        System.out.println("\nInserting planes");
        System.out.println("====================");
        this.planeRepository.saveAll(this.planes.values())
                .stream()
                .forEach(System.out::println);
        System.out.println("====================\n");
    }

    private void createMechanics() {
        this.mechanics.put(EMPLOYEE_CODE_7, new Mechanic(null, EMPLOYEE_CODE_7, "Alvaro", "Martín", COMPANY_NAME_1, 2005, "Advanced technician"));
        this.mechanics.put(EMPLOYEE_CODE_8, new Mechanic(null, EMPLOYEE_CODE_8, "David", "Morata", COMPANY_NAME_2, 2010, "Superior Technician in Aeromechanical Maintenance"));
        this.mechanics.put(EMPLOYEE_CODE_9, new Mechanic(null, EMPLOYEE_CODE_9, "Samuel", "Baquero", COMPANY_NAME_1, 2008, "Line mechanic"));
        System.out.println("\nInserting mechanics");
        System.out.println("====================");
        this.mechanicRepository.saveAll(this.mechanics.values())
                .stream()
                .forEach(System.out::println);
        System.out.println("====================\n");
    }

    private void createAirports() {
        this.airports.put(CITY_1, new Airport(null, "MAD", "Adolfo Suárez Madrid–Barajas",
                CITY_1, "Spain"));
        this.airports.put(CITY_2, new Airport(null, "VAL", "Manises", CITY_2, "Spain"));
        this.airports.put(CITY_3, new Airport(null, "SCQ", "Rosalía de Castro - Santiago de Compostela",
                CITY_3, "Spain"));
        System.out.println("\nInserting airports");
        System.out.println("====================");
        this.airportRepository.saveAll(this.airports.values())
                .stream()
                .forEach(System.out::println);
        System.out.println("====================\n");
    }

    private void createMaintenances() throws ParseException {

        // Boeing plane maintenances
        Maintenance boeingReparationMaintenanceValencia = new Maintenance(null, this.planes.get(PLANE_REGISTRATION_1),
                this.sdf.parse("26/01/2020 17:15:32"), this.sdf.parse("26/01/2020 18:45:32"),
                1.5f, this.mechanics.get(EMPLOYEE_CODE_7), "reparation", "landing gear failure", this.airports.get(CITY_2));
        Maintenance boeingMaintenanceMadrid = new Maintenance(null, this.planes.get(PLANE_REGISTRATION_1),
                this.sdf.parse("12/06/2020 17:15:32"), this.sdf.parse("12/06/2020 17:15:32"),
                1.5f, this.mechanics.get(EMPLOYEE_CODE_9), "periodic", "Periodic maintenance", this.airports.get(CITY_1));
        Maintenance boeingPeriodicMaintenanceValencia = new Maintenance(null, this.planes.get(PLANE_REGISTRATION_1),
                this.sdf.parse("12/06/2021 12:00:00"), this.sdf.parse("12/06/2020 13:00:00"),
                1.0f, this.mechanics.get(EMPLOYEE_CODE_7), "reparation", "landing gear failure", this.airports.get(CITY_2));

        // Airbus plane maintenances
        Maintenance airbusMaintenanceMadrid = new Maintenance(null, this.planes.get(PLANE_REGISTRATION_2),
                this.sdf.parse("22/04/2020 11:15:32"), this.sdf.parse("22/04/2020 12:15:32"),
                1.5f, this.mechanics.get(EMPLOYEE_CODE_8), "periodic", "Periodic maintenance", this.airports.get(CITY_1));
        Maintenance airbusMaintenanceValencia = new Maintenance(null, this.planes.get(PLANE_REGISTRATION_2),
                this.sdf.parse("22/11/2020 12:00:00"), this.sdf.parse("22/11/2020 13:00:00"),
                1.0f, this.mechanics.get(EMPLOYEE_CODE_9), "reparation", "mechanical failures", this.airports.get(CITY_2));

        // Embraer plane maintenances
        Maintenance embraerMaintenanceSantiago = new Maintenance(null, this.planes.get(PLANE_REGISTRATION_3),
                this.sdf.parse("16/01/2020 09:30:10"), this.sdf.parse("16/01/2020 12:00:10"),
                2.5f, this.mechanics.get(EMPLOYEE_CODE_8), "reparation", "mechanical failures", this.airports.get(CITY_3));

        System.out.println("\nInserting maintenances");
        System.out.println("====================");
        this.maintenanceRepository.saveAll(
                Arrays.asList(
                        boeingReparationMaintenanceValencia,
                        boeingMaintenanceMadrid,
                        boeingPeriodicMaintenanceValencia,
                        airbusMaintenanceMadrid,
                        airbusMaintenanceValencia,
                        embraerMaintenanceSantiago
                ))
                .stream()
                .forEach(System.out::println);
        System.out.println("====================\n");
    }

    private void createCabinCrew() {
        this.cabinCrew.add(new CabinCrew(null, EMPLOYEE_CODE_1, "Alberto", "Eyo", COMPANY_NAME_1, "Commander"));
        this.cabinCrew.add(new CabinCrew(null, EMPLOYEE_CODE_2, "Pelayo", "Martín", COMPANY_NAME_1, "Co-pilot"));
        this.cabinCrew.add(new CabinCrew(null, EMPLOYEE_CODE_3, "Luis", "Perez", COMPANY_NAME_2, "Commander"));
        this.cabinCrew.add(new CabinCrew(null, EMPLOYEE_CODE_4, "Isabel", "Ayuso", COMPANY_NAME_2, "Co-pilot"));
        this.cabinCrew.add(new CabinCrew(null, EMPLOYEE_CODE_5, "María", "López", COMPANY_NAME_2, "Commander"));
        this.cabinCrew.add(new CabinCrew(null, EMPLOYEE_CODE_6, "Ana", "García", COMPANY_NAME_2, "Co-pilot"));
        System.out.println("\nInserting cabin crew without flights");
        System.out.println("====================");
        CabinCrew cabinCrewWithoutFlights = new CabinCrew(null, EMPLOYEE_CODE_0, "Abel", "Hernández", COMPANY_NAME_3, "Co-pilot");
        this.cabinCrew.add(cabinCrewWithoutFlights);
        System.out.println(this.cabinCrewRepository.save(cabinCrewWithoutFlights));
        System.out.println("====================");
    }

    private List<CabinCrew> getCabinCrewByCompany(String company) {
        return this.cabinCrew
                .stream()
                .filter(cc -> cc.getCompanyName().equals(company))
                .collect(Collectors.toList());
    }

    private void createFlights() throws ParseException {

        Flight flightMadridValencia = new Flight(null, FLIGHT_CODE_1, COMPANY_NAME_1, this.planes.get(PLANE_REGISTRATION_1),
                this.airports.get(CITY_1), this.airports.get(CITY_2),
                this.sdf.parse("06/02/2021 13:23:45"), 1.05f);
        this.linkCrew(flightMadridValencia, this.getCabinCrewByCompany(COMPANY_NAME_1));
        this.flights.put(FLIGHT_CODE_1, flightMadridValencia);

        Flight madridSantiago = new Flight(null, FLIGHT_CODE_2, COMPANY_NAME_2, this.planes.get(PLANE_REGISTRATION_1),
                this.airports.get(CITY_1), this.airports.get(CITY_3),
                sdf.parse(DEPARTURE_DATE_1 + " 01:00:00"), 2.15f);
        this.linkCrew(madridSantiago, this.getCabinCrewByCompany(COMPANY_NAME_2).subList(0, 2));
        this.flights.put(FLIGHT_CODE_2, madridSantiago);

        Flight flightValenciaSantiago = new Flight(null, FLIGHT_CODE_3, COMPANY_NAME_2, this.planes.get(PLANE_REGISTRATION_2),
                this.airports.get(CITY_2), this.airports.get(CITY_3),
                sdf.parse(DEPARTURE_DATE_1 + " 02:00:00"), 3.5f);
        this.linkCrew(flightValenciaSantiago, this.getCabinCrewByCompany(COMPANY_NAME_2).subList(2, 4));
        this.flights.put(FLIGHT_CODE_3, flightValenciaSantiago);

        Flight secondFlightMadridSantiago = new Flight(null, FLIGHT_CODE_4, COMPANY_NAME_2, this.planes.get(PLANE_REGISTRATION_3),
                this.airports.get(CITY_1), this.airports.get(CITY_3),
                sdf.parse(DEPARTURE_DATE_1 + " 03:00:00"), 2.25f);
        this.linkCrew(secondFlightMadridSantiago, this.getCabinCrewByCompany(COMPANY_NAME_2).subList(0, 2));
        this.flights.put(FLIGHT_CODE_4, secondFlightMadridSantiago);

        Flight flightValenciaMadrid = new Flight(null, FLIGHT_CODE_5, COMPANY_NAME_2, this.planes.get(PLANE_REGISTRATION_1),
                this.airports.get(CITY_2), this.airports.get(CITY_1),
                this.sdf.parse(DEPARTURE_DATE_1_WITH_HOUR), 1.05f);
        this.linkCrew(flightValenciaMadrid, this.getCabinCrewByCompany(COMPANY_NAME_1).subList(0, 1));
        this.flights.put(FLIGHT_CODE_5, flightValenciaMadrid);


        System.out.println("\nInserting flights and cabin crew");
        System.out.println("====================");
        this.flightRepository.saveAll(this.flights.values())
                .stream()
                .forEach(this::printFlightWithCabinCrew);
        System.out.println("====================\n");
    }

    private void printFlightWithCabinCrew(Flight flight) {
        System.out.println(flight);
        flight.getCabinCrew()
                .stream()
                .forEach(flightCabinCrew -> System.out.println("\t" + flightCabinCrew.getCabinCrew()));
    }

    private void initEntities() {
        this.cabinCrew = new ArrayList<>();
        this.planes = new HashMap<>();
        this.mechanics = new HashMap<>();
        this.airports = new HashMap<>();
        this.flights = new HashMap<>();
    }

    private Flight linkCrew(Flight flight, List<CabinCrew> cabinCrew) {
        List<FlightCabinCrew> cabinCrewInFlight = new ArrayList<>();
        for (CabinCrew cc : cabinCrew) {
            cabinCrewInFlight.add(new FlightCabinCrew(flight, cc));
        }
        flight.setCabinCrew(cabinCrewInFlight);
        return flight;
    }

    public Mechanic getMechanicByEmployeeCode(String employeeCode) {
        return this.mechanics.get(employeeCode);
    }

}
