package es.urjc.code.practice.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString(callSuper = true)
public class Mechanic extends Employee {

    private Integer incorporationYear;

    @NotBlank
    private String formation;

    public Mechanic(Long id, String employeeCode, String name, String surname, String companyName,
                    Integer incorporationYear, String formation) {
        super(id, employeeCode, name, surname, companyName);
        this.incorporationYear = incorporationYear;
        this.formation = formation;
    }
}
