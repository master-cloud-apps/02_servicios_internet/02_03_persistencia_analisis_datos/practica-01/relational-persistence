package es.urjc.code.practice.services;

import es.urjc.code.practice.models.Flight;
import es.urjc.code.practice.utils.DatabaseLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.util.CollectionUtils.isEmpty;

@SpringBootTest
class FlightServiceTest {

    @Autowired
    private FlightService flightService;
    @Autowired
    private DatabaseLoader prepareDatabase;

    @BeforeEach
    void setUp() throws ParseException {
        this.prepareDatabase.setUp();
    }

    @AfterEach
    void tearDown() {
        this.prepareDatabase.deleteAll();
    }

    @Test
    void givenNotExistsDestinationCityFlightsInPassedDate_whenGetFlightsToCity_thenShouldReturnEmptyList() {
        List<Flight> flights = this.flightService
                .findFlightsByDestinationCityInDate(
                        this.prepareDatabase.getFlights().get(DatabaseLoader.FLIGHT_CODE_1).getDestinationAirport().getCity(),
                        new Date());

        assertTrue(isEmpty(flights));
    }

    @Test
    void givenExistsFlightsInPassedDateButNotForCity_whenGetFlightsToCity_thenShouldReturnEmptyList() {
        List<Flight> flights = this.flightService
                .findFlightsByDestinationCityInDate(
                        "Utopia",
                        this.prepareDatabase.getFlights().get(DatabaseLoader.FLIGHT_CODE_1).getDepartureDate());

        assertTrue(isEmpty(flights));
    }

    @Test
    void givenExistsDestinationCityFlightsInPassedDate_whenGetFlightsToDestinationCity_thenShouldReturnFlights() {
        Flight flight = this.prepareDatabase.getFlights().get(DatabaseLoader.FLIGHT_CODE_1);
        List<Flight> flights = this.flightService
                .findFlightsByDestinationCityInDate(flight.getDestinationAirport().getCity(),
                        flight.getDepartureDate());

        assertFalse(isEmpty(flights));
        assertEquals(1, flights.size());
    }

    @Test
    void givenExistsSeveralDestinationCityFlightsInPassedDate_whenGetFlightsToDestinationCity_thenShouldReturnFlightsOrderedByHour() throws ParseException {
        List<Flight> flights = this.flightService
                .findFlightsByDestinationCityInDate(DatabaseLoader.CITY_3, this.prepareDatabase.getSdf()
                        .parse(DatabaseLoader.DEPARTURE_DATE_1_WITH_HOUR));

        assertFalse(isEmpty(flights));
        assertEquals(3, flights.size());

        assertEquals(DatabaseLoader.CITY_3, flights.get(0).getDestinationAirport().getCity());
        assertEquals(DatabaseLoader.CITY_3, flights.get(1).getDestinationAirport().getCity());
        assertEquals(DatabaseLoader.CITY_3, flights.get(2).getDestinationAirport().getCity());

        assertTrue(this.prepareDatabase.getSdf().format(flights.get(0).getDepartureDate()).startsWith(DatabaseLoader.DEPARTURE_DATE_1));
        assertTrue(this.prepareDatabase.getSdf().format(flights.get(1).getDepartureDate()).startsWith(DatabaseLoader.DEPARTURE_DATE_1));
        assertTrue(this.prepareDatabase.getSdf().format(flights.get(2).getDepartureDate()).startsWith(DatabaseLoader.DEPARTURE_DATE_1));

        assertTrue(flights.get(0).getDepartureDate().before(flights.get(1).getDepartureDate()));
        assertTrue(flights.get(1).getDepartureDate().before(flights.get(2).getDepartureDate()));

    }

}
